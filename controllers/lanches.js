var lanches =[
    {nome:'X-Bacon', referencia:'x-bacon', ingredientes:['bacon', 'hambúrguer de carne', 'queijo']},
    {nome:'X-Burger', referencia:'x-burger', ingredientes:['hambúrguer de carne', 'queijo']},
    {nome:'X-Egg', referencia:'x-egg', ingredientes:['ovo', 'hambúrguer de carne', 'queijo']},
    {nome:'X-Egg Bacon', referencia:'x-egg-bacon', ingredientes:['ovo', 'bacon', 'hambúrguer de carne', 'queijo']}
];

var ingredientes=[
    {nome:'alface', preco: 0.4, quantidade:0},
    {nome:'bacon', preco:2, quantidade:0},
    {nome:'hambúrguer de carne', preco:3, quantidade:0},
    {nome:'ovo', preco:0.8, quantidade:0},
    {nome:'queijo', preco:1.5, quantidade:0},
];

function inArrayObject(array,share,position)
{
    for(a in array)
    {
        if(array[a][position]===share)
        {
            return array[a];
        }
    }
    return {};
}


var controller={

    Lanches:function(req, res)
    {
        for(lanche of lanches)
        {
            lanche.preco=0;
            for(ingrediente of lanche.ingredientes)
            {
                lanche.preco+=inArrayObject(ingredientes,ingrediente,'nome').preco;
            }
        }

        res.json({lanches:lanches});
    },

    Lanche:function(req, res)
    {
        var id=req.params.id;
        var lanche = inArrayObject(lanches,id,'referencia');

        var ingrediente;
        lanche.lista=[];
        for(ing of ingredientes)
        {
            ingrediente=Object.assign({}, ing);
            if(lanche.ingredientes.indexOf(ingrediente.nome)!==-1)
            {
                ingrediente.quantidade++;
            }
            lanche.lista.push(ingrediente);
        }

        res.json({lanche:lanche});
    },
    calculoPrecos:function(req, res)
    {
        var preco=0;
        var promocoes=[];
        var itens=[];

        for(ingrediente of req.body.ingredientes)
        {
            if (ingrediente.quantidade) {
                var sub=0;
                if (ingrediente.nome==='hambúrguer de carne') {
                    sub=ingrediente.quantidade/3|0;
                    if(sub) promocoes.push('Muita carne');
                }else if (ingrediente.nome==='queijo') {
                    sub=ingrediente.quantidade/3|0;
                    if(sub) promocoes.push('Muito queijo');
                }
                itens.push(ingrediente.nome);
                preco+=inArrayObject(ingredientes,ingrediente.nome,'nome').preco*(ingrediente.quantidade-sub);
            }
        }

        if (itens.indexOf('alface')!==-1 && itens.indexOf('bacon')===-1) {
            promocoes.push('Light');
            preco=preco*0.9;
        }


        res.json({preco:preco, promocoes:promocoes});

    }



};

module.exports = controller;