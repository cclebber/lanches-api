var express = require('express');
var lanches = require('../controllers/lanches');
var router = express.Router();


router.get('/', function(req, res){

    res.json({ok:true});
    
});

//retrna a array de anches
router.get('/lanches', lanches.Lanches);
//recebe  id d anche e retrna a ista de ingredientes e suas quantidades predefinidas
router.get('/lanches/:id',lanches.Lanche);
//recebe s ingredientes e suas quantidades e faz  cacu d anche e devids descnts
router.put('/lanches/atualiza', lanches.calculoPrecos);


module.exports = router;