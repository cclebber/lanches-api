var express = require('express');
var rotas = require('./routes/rotas');
var app = express();

app.use(express.json());
app.use(function(req, res, next) {
    res.setHeader('Access-Control-Allow-Origin', 'http://localhost:4000');    
    res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE');
    res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,content-type');
    next();    
})

app.use(rotas);

app.listen(3000, function () {

    console.log('ligado na porta 3000');

});

module.exports = app;