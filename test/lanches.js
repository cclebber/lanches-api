var chai = require('chai');
var server  = require('../index.js');
var chaiHttp = require('chai-http');
var lanches = require('../controllers/lanches');

var should = chai.should();

chai.use(chaiHttp);


describe('Lanches', function() {


    var lanche={};

    it('Get Lanches', function(done) 
    {        
        chai.request(server).get('/lanches').end(function(err, res) {

            res.should.have.status(200);
            res.body.should.have.property('lanches');
            lanche=res.body.lanches[0];

            done();
        })        
    })

    it('Get Lanche', function(done) 
    {        
        chai.request(server).get('/lanches/'+lanche.referencia).end(function(err, res) {

            res.should.have.status(200);

            res.body.should.have.property('lanche');

            res.body.lanche.should.have.property('nome');
            res.body.lanche.should.have.property('referencia');
            res.body.lanche.should.have.property('ingredientes');
            res.body.lanche.should.have.property('lista');

            lanche=res.body.lanche;
            done();
        })        
    })

    it('Get Preço', function(done) 
    {        
        chai.request(server).put('/lanches/atualiza').send({ingredientes:lanche.lista}).end(function(err, res) {

            res.should.have.status(200);
            res.body.should.have.property('preco');
            res.body.should.have.property('promocoes');

            done();
        })        
    })

    it('Get Preço Promoção 1', function(done) 
    {   
        var ingredientes=[
            {'nome':'alface', 'quantidade':1},
            {'nome':'bacon', 'quantidade':0},
        ]
        chai.request(server).put('/lanches/atualiza').send({ingredientes:ingredientes}).end(function(err, res) {

            res.should.have.status(200);
            res.body.should.have.property('preco');
            res.body.should.have.property('promocoes');
            res.body.promocoes.should.have.be.an('array').that.includes('Light');

            done();
        })        
    })

    it('Get Preço Promoção 2', function(done) 
    {   
        var ingredientes=[
            {'nome':'hambúrguer de carne', 'quantidade':3},
        ]
        chai.request(server).put('/lanches/atualiza').send({ingredientes:ingredientes}).end(function(err, res) {

            res.should.have.status(200);
            res.body.should.have.property('preco');
            res.body.should.have.property('promocoes');
            res.body.promocoes.should.have.be.an('array').that.includes('Muita carne');

            done();
        })        
    })

    it('Get Preço Promoção 3', function(done) 
    {   
        var ingredientes=[
            {'nome':'queijo', 'quantidade':3},
        ]
        chai.request(server).put('/lanches/atualiza').send({ingredientes:ingredientes}).end(function(err, res) {

            res.should.have.status(200);
            res.body.should.have.property('preco');
            res.body.should.have.property('promocoes');
            res.body.promocoes.should.have.be.an('array').that.includes('Muito queijo');

            done();
        })        
    })

    
})